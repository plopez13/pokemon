let pokemons = [
    {
        nombre:"Bulbasaur",
        defensa: 100,
        ataque:100,
        ataques:[
            0,
            3
        ],
        tipo:0,
        energia: 100,
        imagenFrente:"Bulbasaur_frente.png",
        imagenEspalda:"Bulbasaur_espalda.png"
    },
    {
        nombre:"Squirtle",
        defensa: 200,
        ataque:100,
        ataques:[
            0,
            1
        ],
        tipo:2,
        energia: 100,
        imagenFrente:"Squirtle_frente.png",
        imagenEspalda:"Squirtle_espalda.png"
    },
    {
        nombre:"Charmander",
        defensa: 100,
        ataque:100,
        ataques:[
            0,
            2
        ],
        tipo:1,
        energia: 90,
        imagenFrente:"Charmander_frente.png",
        imagenEspalda:"Charmander_espalda.png"
    }
]

let tipos = ["planta", "fuego", "agua", "normal"]

let ataques = [
    {
        nombre:"placaje",
        tipo:3,
        potencia:80
    },
    {
        nombre:"pistola de agua",
        tipo: 2,
        potencia:100
    },
    {
        nombre:"lanza llamas",
        tipo: 1,
        potencia:100
    },
    {
        nombre:"hojas navaja",
        tipo: 0,
        potencia:100
    },

]

let seleccionadoRival = 1
let seleccionadoJugador = 2

let energiaRival = pokemons[seleccionadoRival].energia
let energiaJugador = pokemons[seleccionadoJugador].energia

function cargar() {

    let botonesAtaques = ""
    for (let index = 0; index < pokemons[seleccionadoJugador].ataques.length; index++) {
        botonesAtaques = botonesAtaques + `<button onclick="ataqueSeleccionando(${pokemons[seleccionadoJugador].ataques[index]})">${ataques[pokemons[seleccionadoJugador].ataques[index]].nombre}</button>`        
    }

    let contenedor = `       
    <div class="rival">
        <div class="rival__datos datos">
            <div class="rival__datos__nombre nombre">${pokemons[seleccionadoRival].nombre}</div>
            <div class="rival__datos__energia energia">
            <div class="rival__datos__energia__total"></div>
            </div>     
        <div class="rival__datos__contador contador">${energiaRival + "-" + pokemons[seleccionadoRival].energia}</div>                           
        </div>
        <img src="./img/${pokemons[seleccionadoRival].imagenFrente}" alt="" class="rival__foto foto">
    </div>
    <div class="jugador">
        <div class="jugador__datos datos">
            <div class="jugador__datos__nombre nombre">${pokemons[seleccionadoJugador].nombre}</div>
            <div class="jugador__datos__energia energia">
                <div class="jugador__datos__energia__total"></div>
            </div> 
        <div class="rival__datos__contador contador">${energiaJugador + "-" + pokemons[seleccionadoJugador].energia}</div>                               
        </div>
        <img src="./img/${pokemons[seleccionadoJugador].imagenEspalda}" alt="" class="jugador__foto foto">
        <div class="botones">
        ${botonesAtaques}
        </div>
    </div>`

    document.querySelector(".contenedor").innerHTML = contenedor;
}

function ataqueSeleccionando(params) {
    //en el params recibo la posición del ataque recibido.
    alert("elegiste: " + params)
    //trabajar en esta área.
}

cargar()
